<?php
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;
use Phalcon\Mvc\Model\Criteria;

const URL = "http://localhost/Api_unicornio/api/unicornio";

class IndexController extends ControllerBase
{

    public function indexAction()
    {

    }

    /**
     * Searches for unicornio
     */
    public function searchAction()
    {
        $numberPage = 1;
        if (!$this->request->isPost()) {
            $numberPage = $this->request->getQuery("page", "int");
        }else {
          $this->persistent->id = $this->request->getPost("Id");
          $this->persistent->nombre = $this->request->getPost("Nombre");
        }

        $url = URL;
        if($this->persistent->id)
        {
          $url = URL."/".$this->persistent->id;
        }else if($this->persistent->nombre) {
          $url = URL."/".$this->persistent->nombre;
        }

         $json = file_get_contents($url);
         $data = json_decode($json,true);

          $paginator = new PaginatorArray([
           'data' => $data,
           'limit'=> 10,
           'page' => $numberPage
          ]);

        $this->view->page = $paginator->getPaginate();
    }




}
